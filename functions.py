import shelve
from random import randrange
from json import loads, dumps
from pyautogui import locateOnScreen, moveTo, dragTo, click
from time import sleep

def get_logo_size(logo=''):
    """Retorna las dimensiones del logo.

    Args:

        logo (str): ruta del archivo

    Returns: (tuple, False)"""

    res = tuple()

    try:
        logo_info = locateOnScreen(logo, grayscale=True)
        res += (logo_info[2], logo_info[3])
    except TypeError:
        res = False

    return res

def get_logo_location(logo=''):
    """Retorna la localizacion del logo.

    Args:

        logo (str): ruta del archivo

    Returns: (tuple, False)"""

    res = tuple()

    try:
        logo_info = locateOnScreen(logo, grayscale=True)

        x = logo_info[0]
        y = logo_info[1]
        x1 = 0
        y1 = 0

        logo_with = logo_info[2]
        logo_height = logo_info[3]

        tmp_acc = 0

        # calculamos x1 en base a la anchura del logo
        x1 = x

        while tmp_acc <= logo_with:
            tmp_acc += 1
            x1 += 1

        tmp_acc = 0

        # calculamos x1 en base a la anchura del logo
        y1 = y

        while tmp_acc <= logo_height:
            tmp_acc += 1
            y1 += 1

        res += (x, y, x1, y1)


    except TypeError:
        res = False

    return res

def is_test_img(img_path=''):
    """Determina si una imagen es o no de prueba.

    Args:

        img_path (str): ruta del archivo

    Returns: (bool)"""

    res = True

    try:
        img_data = locateOnScreen(img_path, grayscale=True)

        if img_data is None:
            res = False
    except TypeError:
        res = False
        print "TypeError: el parametro logo debe ser de tipo str"

    return res

def draw_box(location=(), duration=1):
    """Encierra un logo en la localizacion especificada.

    Args:

        location (tuple): coordenadas x, y, x1, y1 del logo
        duration (int, float, optional): duracion total del movimiento, default 1 seg

    Returns: (bool)"""

    res = True
    time_per_movement = duration / 2

    try:
        moveTo(location[0], location[1], time_per_movement)
        dragTo(location[2], location[3], time_per_movement)
        # moveTo(location[2], location[3], time_per_movement)
    except (IndexError, TypeError):
        res = False

    return res


def draw_random_boxes(max_movs=(), max_boxes=3, duration=1, nothing_location=()):
    """Dibuja cajas al azar.

    Args:

        max_movs (tuple): movimientos max y min en X/Y
        max_logo (int): cantidad max de cajas a dibujar
        duration (int, float, optional): duracion total del movimiento, default 1 seg
        nothing_location (tuple): ubicacion del checkbox nothing to bound

    Returns: (bool)"""

    res = True
    time_per_movement = duration / 2
    min_x, max_x, min_y, max_y = max_movs

    probabilities = list()

    for i in range(20):
        if i == 0:
            probabilities.append(0)
        elif i == 10:
            probabilities.append(max_boxes - 2)
        elif i == 19:
            probabilities.append(max_boxes - 1)
        else:
            probabilities.append(max_boxes)

    num_of_boxes_to_draw = probabilities[randrange(0, len(probabilities))]

    if num_of_boxes_to_draw > 0:
        for i in range(1, num_of_boxes_to_draw + 1):
            try:
                moveTo(randrange(min_x, max_x), randrange(range(min_y * i, (max_y / num_of_boxes_to_draw) * i)[0] , (max_y / num_of_boxes_to_draw) * i), time_per_movement)
                dragTo(randrange(min_x, max_x), randrange(range(min_y * i, (max_y / num_of_boxes_to_draw) * i)[0] , (max_y / num_of_boxes_to_draw) * i), time_per_movement)
                # moveTo(randrange(min_x, max_x), randrange(range(min_y * i, (max_y / num_of_boxes_to_draw) * i)[0] , (max_y / num_of_boxes_to_draw) * i), time_per_movement)

            except (IndexError, TypeError):
                res = False
    else:
        sleep(.4)
        # click(nothing_location[0], nothing_location[1])

    return res

def save_test_img(test_img, location=()):
    """Guarda localizacion de un logo en su respectivo testImg.

    Args:

        test_img (str): identificador del test_img
        location (str): coordenadas x, y, x1, y1 del logo

    Returns: (bool)"""

    res = True

    try:
        d = shelve.open('locations.db')

        if d.has_key(test_img):
            d[test_img] = d[test_img] + [[
                location[0],
                location[1],
                location[2],
                location[3]]]
        else:
            d[test_img] = [
                location[0],
                location[1],
                location[2],
                location[3]]

            d[test_img] = [d[test_img], ]

    except:
        res = False

    return res

def get_test_img(test_img):
    """Retorna la informacion de un test img si existe.

    Args:

        test_img (str): identificador del test_img

    Returns: (list, False)"""

    res = True

    try:
        d = shelve.open('locations.db')

        res = d[test_img]

    except:
        res = False

    return res
