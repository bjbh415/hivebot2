from random import randrange
from time import sleep
from os import listdir, walk
from json import loads
from pyautogui import press, size, click
from pynput import keyboard
from functions import get_logo_location, is_test_img, draw_box,\
    draw_random_boxes, save_test_img, get_test_img

# ancho/alto de la pantalla
screenWith, screenHeight = size()

# margenes
MARGIN_LEFT = 10
MARGIN_RIGHT = 270
MARGIN_TOP = 280
MARGIN_BOTTOM = 0

# posicion X/Y de "Nada que deliminar"
NOTHING_TO_BOUND_X = screenWith - 40
NOTHING_TO_BOUND_Y = 140

# restar movimiento dentro del campo
OFFSET_X = 300
OFFSET_Y = 0

# restamos los margenes al area de imagenes
IMG_AREA_WITH = screenWith - MARGIN_LEFT - MARGIN_RIGHT
IMG_AREA_HEIGHT = screenHeight - MARGIN_TOP - MARGIN_BOTTOM
IMG_AREA_CENTER_X = (IMG_AREA_WITH + MARGIN_LEFT) / 2
IMG_AREA_CENTER_Y = (IMG_AREA_HEIGHT + MARGIN_TOP) / 2

# movimientos maximos y minimos X/Y
MIN_MOV_X = IMG_AREA_CENTER_X - (IMG_AREA_WITH / 2) + OFFSET_X
MAX_MOV_X = IMG_AREA_CENTER_X + (IMG_AREA_WITH / 2) - OFFSET_X
MIN_MOV_Y = IMG_AREA_CENTER_Y - (IMG_AREA_HEIGHT / 2) + OFFSET_Y
MAX_MOV_Y = IMG_AREA_CENTER_Y + (IMG_AREA_HEIGHT / 2)  - OFFSET_Y

# carga la configuracion general del programa
GENERAL_CONFIG = loads(open('./config.json', 'r').read())

# obtine lista de test_imgs
TEST_IMGS_LIST = tuple(listdir(GENERAL_CONFIG['TEST_IMGS_DIR']))

# detiene el loop del programa
stop = False

# una breve pausa antes de empezar el programa
""" print "Comienza en 3 segundos ..."
sleep(3)
print "Ya!" """

def on_press(key):
    global stop

    if key == keyboard.Key.esc or key == keyboard.Key.ctrl or key == keyboard.Key.ctrl_r:
        stop = True
        return False

with keyboard.Listener(on_press=on_press) as listener:
    while stop != True:
	sleep(GENERAL_CONFIG['BEFORE_DRAWING_PAUSE_DURATION'])

        test_img = None

        """ for img in TEST_IMGS_LIST:
            img_path = './{0}/{1}/{1}.png'.format(GENERAL_CONFIG['TEST_IMGS_DIR'], img)

            if is_test_img(img_path):
                test_img = img
                break """

        if test_img:
            LOGOS_LIST = listdir('./{0}/{2}/{1}/'.format(GENERAL_CONFIG['TEST_IMGS_DIR'], \
                GENERAL_CONFIG['TEST_IMGS_LOGOS_DIR'], test_img))

            if LOGOS_LIST:
                logo_index = 0

                for logo in LOGOS_LIST:
                    test_img_locations = get_test_img(str(test_img))
                    if test_img_locations and len(test_img_locations) == len(LOGOS_LIST):
                        draw_box(tuple(test_img_locations[logo_index]), GENERAL_CONFIG['MOVEMENTS_DURATION'])
                    else:
                        logo_location = get_logo_location('./{0}/{2}/{1}/{3}'\
                            .format(GENERAL_CONFIG['TEST_IMGS_DIR'],\
                                GENERAL_CONFIG['TEST_IMGS_LOGOS_DIR'], test_img, logo))

                        draw_box(logo_location, GENERAL_CONFIG['MOVEMENTS_DURATION'])
                        save_test_img(str(test_img), logo_location)

                    logo_index += 1
            # else:
                # click(NOTHING_TO_BOUND_X, NOTHING_TO_BOUND_Y)
        else:
            draw_random_boxes((MIN_MOV_X, MAX_MOV_X, MIN_MOV_Y, MAX_MOV_Y),\
                GENERAL_CONFIG['MAX_AMOUNT_OF_LOGOS_TO_BOUND'], GENERAL_CONFIG['MOVEMENTS_DURATION'],\
                (NOTHING_TO_BOUND_X, NOTHING_TO_BOUND_Y))

        sleep(GENERAL_CONFIG['AFTER_DRAWING_PAUSE_DURATION'])
        press("enter")

    listener.join()
